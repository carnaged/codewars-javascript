function reverseString(s) {
  return s.split('').reverse().join('')
}

function sumStrings(a,b) { 
  let i =0;
  let carry = 0;
  let sum = []
  a = a.split('').reverse().join('')
  b = b.split('').reverse().join('')
  while( carry !=0 || i<a.length || i<b.length ){
    let dA = i < a.length ? parseInt(a[i]) : 0;
    let dB = i < b.length ? parseInt(b[i]) : 0;
    // console.log(dA)
    let partialSum = dA + dB + carry;
    carry = Math.floor(partialSum/10);
    sum.push((partialSum%10).toString());
    i++;
  }
  sum.reverse()
  while(sum[0] == '0') sum.shift();
  return sum.join('')
}


module.exports = sumStrings;